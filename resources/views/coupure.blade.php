@extends('welcome')



@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-th-list"></i> Coupure-Activation</h1>
        @if (session('success'))
        <div class="alert alert-success">
            {{session('success')}}
        </div>
      @endif
      @if (session('incorrect'))
        <div class="alert alert-warning">
            {{session('incorrect')}}
        </div>
      @endif


    </div>

    <ul class="app-breadcrumb breadcrumb side">
        <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
        <li class="breadcrumb-item">Zmobile</li>
        <li class="breadcrumb-item active"><a href="#">Coupure-Activation</a></li>
    </ul>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="tile-body">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>

                                <th>Matricule </th>
                                <th>Telephone</th>
                                <th>Position</th>
                                <th>Action</th>

                            </tr>
                        </thead>
                        <tbody>


                            @for ($i = 0; $i < count($information) ; $i++)
                               <tr>
                                <td>{{$information[$i]["name"]}} </td>
                                <td>{{$information[$i]["phone"]}} </td>
                                <td> <a href="https://www.google.com/maps/search/?api=1&query={{$data[$i]["latitude"]}},{{$data[$i]["longitude"]}}" target="_blank"> Voir Position</a>  </td>
                                <td class="row">
                                    <form method="post" action="{{route('sendmsg')}}" >
                                        @csrf
                                           <input type="hidden" class="form-control" name="num"  value={{$information[$i]["phone"]}}>
                                           <input type="hidden" class="form-control" name="msg"  value="555">
                                           <button class="btn btn-danger" type="submit">Bloquer</button>

                                    </form>
                                    <form method="post" action="{{route('sendmsg')}}" >
                                        @csrf
                                           <input type="hidden" class="form-control" name="num"  value={{$information[$i]["phone"]}}>
                                           <input type="hidden" class="form-control" name="msg"  value="666">
                                           <button class="btn btn-primary" type="submit">Debloquer</button>
                                    </form>
                                </td>

                               </tr>
                            @endfor

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



@section('js_special')

<script type="text/javascript" src="design2/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="design2/js/plugins/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    $('#sampleTable').DataTable();
</script>
@endsection

<?php

namespace App\Http\Controllers;

use App\Gateway;
use App\User;
use Illuminate\Http\Request;

class GatewayController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $gateway = Gateway::all();
        if (count($gateway) == 0) {
            $gateway = [];
            return view("gateway",compact("gateway"));
        }else{
            return view("gateway",compact("gateway"));
        }

    }

    public function addgateway(Request $request)
    {
        $gateway = new Gateway();
        $gateway["DeviceId"] = $request["deviceId"];
        $gateway["Sim"] = $request["sim"];
        $gateway->save();
        return redirect()->back()->with("success","Device ajouter avec succes");
    }



    public function Deletegateway(Request $request)
    {

          Gateway::destroy($request["id"]);
          return redirect()->back()->with("success","Module supprime avec succes");
    }

    public function user()
    {
        $user = User::all();
        return view("listeuser",compact("user"));

    }
    public function deleteuser(Request $request)
    {
          $infouser = User::where('id',$request["id"])->first();

          if ($infouser["type"] == null) {
            User::destroy($request["id"]);
            return redirect()->back()->with("success","Utilisateur supprimer");
          }

          if ($infouser["type"] == "admin") {
            return redirect()->back()->with("incorrect","Cet utilisateur ne peut etre supprimer vu que c'est un administrateur");
          }

    }
}

@extends('welcome')



@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-th-list"></i> Message pas Envoyes</h1>
        @if (session('success'))
        <div class="alert alert-success">
            {{session('success')}}
        </div>
      @endif
      @if (session('incorrect'))
        <div class="alert alert-warning">
            {{session('incorrect')}}
        </div>
      @endif


    </div>

    <ul class="app-breadcrumb breadcrumb side">
        <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
        <li class="breadcrumb-item">Zmobile</li>
        <li class="breadcrumb-item active"><a href="#">Message non Envoyes</a></li>
    </ul>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="tile-body">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th>Envoye le </th>
                                <th>Etat du message </th>
                                <th>Numero destinataire</th>
                                <th>Message Envoye</th>
                                <th>Renvoyer le sms</th>

                            </tr>
                        </thead>
                        <tbody>
                            @if (count($message) != 0)
                            @foreach ($message["smss"] as $message)
                            @if ($message["status"] == 2 ||$message["status"] == 6 || $message["status"] == 8 || $message["status"] == 9 || $message["status"] == 10 || $message["status"]==11 || $message["status"]==12)

                            <tr>
                                <td>{{$message["senttime"]["date"]}}</td>
                                <td>{{$message["statustitle"]}}</td>
                                <td>{{$message["sendto"]}}</td>
                                <td>{{$message["body"]}}</td>

                                @if ($message["status"] == 8 ||$message["status"] == 9 || $message["status"] == 10 || $message["status"]==11 || $message["status"]==12 )
                                <td>

                                    <form method="post" action="{{route('sendmsg')}}" >
                                        @csrf

                                           <input type="hidden" class="form-control" name="num"  value={{$message["sendto"]}}>
                                           <input type="hidden" class="form-control" name="msg"  value={{$message["body"]}}>

                                           <div class="form-group">
                                             <div class="col-lg-offset-2 col-lg-10 em">
                                                 <button class="btn btn-primary" type="submit">Renvoyer</button>
                                             </div>
                                           </div>



                                    </form>
                                </td>
                                @endif

                                @if ($message["status"] == 2 ||$message["status"] == 6 )
                                <?php
                                    $now = new DateTime();
                                    $nowstring = $now->format('Y-m-d H:i:s');
                                    $bd = $message["senttime"]["date"];
                                    $mewbd =date("Y-m-d H:i:s",strtotime("+5 minutes", strtotime($bd)));
                                    $resultatdate = strtotime($mewbd)- strtotime($nowstring);
                                    if ($resultatdate< 0 ) {
                                        $renvoie = "OK";
                                    }else {
                                        $renvoie = "NON";
                                    }
                                ?>

                                        @if ($renvoie == "OK")
                                              <td><form method="post" action="{{route('sendmsg')}}" >
                                                @csrf

                                                   <input type="hidden" class="form-control" name="num"  value={{$message["sendto"]}}>
                                                   <input type="hidden" class="form-control" name="msg"  value={{$message["body"]}}>

                                                   <div class="form-group">
                                                     <div class="col-lg-offset-2 col-lg-10 em">
                                                         <button class="btn btn-primary" type="submit">Renvoyer</button>
                                                     </div>
                                                   </div>



                                                </form>
                                        </td>
                                        @endif

                                        @if ($renvoie == "NON")
                                              <td>En cours d'envoie</td>
                                        @endif
                                @endif



                            </tr>
                            @endif
                        @endforeach
                            @else
                            <tr>

                                <td colspan="5"><p class="text-center">Pas de message recuperer </p></td>


                            </tr>


                            @endif


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



@section('js_special')

<script type="text/javascript" src="design2/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="design2/js/plugins/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    $('#sampleTable').DataTable();
</script>
<script type="text/javascript">
    /* Formating function for row details */







  </script>

@endsection

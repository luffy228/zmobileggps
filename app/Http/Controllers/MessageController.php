<?php

namespace App\Http\Controllers;

use App\Gateway;
use App\Traccar;
use DateInterval;
use DateTime;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class MessageController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }



    public function messagenosend()
    {
        $client = new Client();
        $traccar = new Traccar();
        $smstoken = Cache::get('tokensms');
        $gateway = Gateway::all();
        if (count($gateway) == 0) {
            $message = [];
            return view("messagenosend",compact("message"));
        } else{
            $deviceid = $gateway[count($gateway)-1]["DeviceId"];
            $sim = $gateway[count($gateway)-1]["Sim"];

             $responseevent = $client->request('GET',$traccar->getSMS_uri()."/getallsms",[
                'headers' => [
                    'content-type' => 'application/json'],
                 'query' =>[
                     'token' =>$smstoken,
                     'device_id' =>$deviceid,
                 ],

             ]);

             $message = json_decode($responseevent->getBody(),true);

             return view("messagenosend",compact("message"));
        }

    }

    public function coupure()
    {
        $client = new Client();
        $traccar = new Traccar();
        $response = $client->request('GET',$traccar->getBase_uri()."/positions",[
               'headers' => [
                              'content-type' => 'application/json'],
                'auth' => [
                    $traccar->getTraccarUser(),
                    $traccar->getTraccarPass()
                ] ,

            ]);
        $data = json_decode($response->getBody(),true);
        $information = [];
        foreach ($data as $dater) {
            $responsedevice = $client->request('GET',$traccar->getBase_uri()."/devices",[
               'headers' => [
                              'content-type' => 'application/json'],
                'auth' => [
                    $traccar->getTraccarUser(),
                    $traccar->getTraccarPass()
                ],
                'query' =>[
                    'id' =>$dater["deviceId"],
                ],
            ]);
            $deviceInfo = json_decode($responsedevice->getBody(),true);

            $informationposition = [
                'name' => $deviceInfo[0]["name"],
                'phone' => $deviceInfo[0]["phone"]
            ];
            array_push($information,$informationposition);

        }

        return view("coupure",compact("data","information"));




    }

    public function recuperation()
    {
        $client = new Client();
        $traccar = new Traccar();
        $response = $client->request('GET',$traccar->getBase_uri()."/positions",[
               'headers' => [
                              'content-type' => 'application/json'],
                'auth' => [
                    $traccar->getTraccarUser(),
                    $traccar->getTraccarPass()
                ] ,

            ]);
        $data = json_decode($response->getBody(),true);

        foreach ($data as $data) {
            $responsedevice = $client->request('GET',$traccar->getBase_uri()."/devices",[
               'headers' => [
                              'content-type' => 'application/json'],
                'auth' => [
                    $traccar->getTraccarUser(),
                    $traccar->getTraccarPass()
                ],
                'query' =>[
                    'id' =>$data["deviceId"],
                ],
            ]);
            $deviceInfo = json_decode($responsedevice->getBody(),true);
            array_push($data,$deviceInfo);

        }
        return view("recuperation",compact("data"));
    }
}

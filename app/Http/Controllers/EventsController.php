<?php

namespace App\Http\Controllers;

use App\Event;
use App\Gateway;
use App\Traccar;
use DateTime;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class EventsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function getevent()
    {
        $events = count(Event::all());
        return response()->json(['count'=>$events]);
    }

    public function getreport(string $groupe)
    {
        $event = Event::all();
        $countevent = count(Event::all());
        $now = new DateTime();
        $nowstring = $now->format('Y-m-d H:i:s');
        $bd = $event[$countevent-1]["date"];
        $mewbd =date("Y-m-d H:i:s",strtotime("+2 minutes", strtotime($bd)));
        $resultatdate = strtotime($mewbd)- strtotime($nowstring);
        $client = new Client();
        $traccar = new Traccar();

        if ($resultatdate > 0 ) {
            $debuter = $event[$countevent-1]["date"];
            $debut = date("Y-m-d\TH:i:s\Z", strtotime($debuter) );
            $finir = $event[$countevent-1]["date"];
            $mewfin =date("Y-m-d H:i:s",strtotime("+1 minutes", strtotime($finir)));
            $fin = date("Y-m-d\TH:i:s\Z", strtotime($mewfin) );

            $responseevent = $client->request('GET', $traccar->getBase_uri()."/reports/events",[
                'headers' => [
                               'content-type' => 'application/json'],
                 'auth' => [
                     $traccar->getTraccarUser(),
                     $traccar->getTraccarPass()
                 ] ,
                 'query' =>[
                     'groupId' =>$groupe,
                     'from' => $debut,
                     'to' => $fin
                 ],
            ]);
            $event[$countevent-1]["date"] = $mewfin;
            $event[$countevent-1]->save();
            return response(json_decode($responseevent->getBody()));
        }
        if ($resultatdate < 0 ) {
            $debuter = $event[$countevent-1]["date"];
            $debut = date("Y-m-d\TH:i:s\Z", strtotime($debuter));
            $finir = new DateTime();
            $fini = $finir->format('Y-m-d H:i:s');
            $fin = date("Y-m-d\TH:i:s\Z", strtotime($fini));
            $responseevent = $client->request('GET', $traccar->getBase_uri()."/reports/events",[
                'headers' => [
                               'content-type' => 'application/json'],
                 'auth' => [
                    $traccar->getTraccarUser(),
                    $traccar->getTraccarPass()
                 ] ,
                 'query' =>[
                     'groupId' =>$groupe,
                     'from' => $debut,
                     'to' => $fin
                 ],

            ]);
            $event[$countevent-1]["date"] = $fini;
            $event[$countevent-1]->save();
            return response(json_decode($responseevent->getBody()));

        }


    }


    public function getdevice(string $id)
    {
        $client = new Client();
        $traccar = new Traccar();
        $responseevent = $client->request('GET',$traccar->getBase_uri()."/devices",[
            'headers' => [
                           'content-type' => 'application/json'],
             'auth' => [
                $traccar->getTraccarUser(),
                $traccar->getTraccarPass()
             ] ,
             'query' =>[
                'id' =>$id,
             ],

         ]);
        return response(json_decode($responseevent->getBody()));


    }

    public function addevent()
    {
        $events = count(Event::all());
        if ($events == 0) {

            $add = new Event();
            $date = new DateTime();
            $add["date"] = $date;
            $add->save();
            return response("ok");

        }
    }

    public function getgroupe()
    {
        $client = new Client();
        $traccar = new Traccar();

        $responseevent = $client->request('GET',$traccar->getBase_uri()."/groups",[
            'headers' => [
                           'content-type' => 'application/json'],
             'auth' => [
                $traccar->getTraccarUser(),
                $traccar->getTraccarPass()
             ]
         ]);
        return response(json_decode($responseevent->getBody()));
    }



    public function sendmsg(Request $request)
    {

        $client = new Client();
        $traccar = new Traccar();
        $smstoken = Cache::get('tokensms');
        $gateway = Gateway::all();
        if (count($gateway)== 0) {
            return response("NULL");
        }else{
            $deviceid = $gateway[count($gateway)-1]["DeviceId"];
            $sim = $gateway[count($gateway)-1]["Sim"];

             $responseevent = $client->request('GET',$traccar->getSMS_uri()."/addsms",[
                'headers' => [
                    'content-type' => 'application/json'],
                 'query' =>[
                     'token' =>$smstoken,
                     'sendto' => $request["num"],
                     'body' => $request["msg"],
                     'device_id' =>$deviceid,
                     'sim'=>$sim,
                     'urgent'=>1
                 ],

             ]);

             return response()->json(json_decode($responseevent->getBody())  );
        }


    }


    public function sendmsgplateforme(Request $request)
    {

        $client = new Client();
        $traccar = new Traccar();
        $smstoken = Cache::get('tokensms');
        $gateway = Gateway::all();
        if (count($gateway)== 0) {
            return redirect()->back()->with("success","Aucun telephone n'a ete enregistrer pour envoyer les messages");
        }else{
            $deviceid = $gateway[count($gateway)-1]["DeviceId"];
            $sim = $gateway[count($gateway)-1]["Sim"];

             $responseevent = $client->request('GET',$traccar->getSMS_uri()."/addsms",[
                'headers' => [
                    'content-type' => 'application/json'],
                 'query' =>[
                     'token' =>$smstoken,
                     'sendto' => $request["num"],
                     'body' => $request["msg"],
                     'device_id' =>$deviceid,
                     'sim'=>$sim,
                     'urgent'=>1
                 ],
             ]);
             return redirect()->back()->with("success","Message renvoyer avec succes");
        }


    }
}

@extends('welcome')



@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-th-list"></i>Liste Utilisateur</h1>

        @if (session('success'))
        <div class="alert alert-success">
            {{session('success')}}
        </div>
      @endif
      @if (session('incorrect'))
        <div class="alert alert-warning">
            {{session('incorrect')}}
        </div>
      @endif
    </div>

    <ul class="app-breadcrumb breadcrumb side">
        <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
        <li class="breadcrumb-item">Zmobile</li>
        <li class="breadcrumb-item active"><a href="#">Utilisateur</a></li>
    </ul>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="tile-body">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Email</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                        @foreach ($user as $user)
                        <tr>
                        <td>{{$user["name"]}}</td>
                        <td>{{$user["email"]}}</td>
                            <td>
                                <span title="Supprimer"><button style="color: red" class="fa fa-trash-o deletemodal" data-toggle="modal"  data-id={{$user->id }}  data-target="#DeleteModal" ></button></span>

                            </td>


                        </tr>
                        @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="DeleteModal" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Supprimer un utilisateur</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <form method="post" action="{{route('deleteuser')}}">
           @csrf
        <div class="modal-body">

              <input type="hidden" class="form-control" name="id" id="idk1" value="">


        </div>
        <div class="modal-footer">
          <button class="btn btn-danger" type="submit">Etes vous sur de vouloir supprimer cet utilisateur?</button>
        </div>
        </form>
      </div>
    </div>
</div>
@section('js_special')

<script type="text/javascript" src="design2/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="design2/js/plugins/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    $('#sampleTable').DataTable();
</script>
<script type="text/javascript">
    /* Formating function for row details */




      $(document).on("click", ".deletemodal", function () {
       var ids = $(this).attr('data-id');
       console.log(ids);
       $(".modal-body #idk1").val( ids );

      });


  </script>

@endsection

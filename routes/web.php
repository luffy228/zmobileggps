<?php

use App\Http\Controllers\DispositifController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Input;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/evenemt', 'EventsController@getevent');

Route::get('/addevent', 'EventsController@addevent');

Route::post('/updatetime', 'EventsController@updatevent');

Route::post('/sendmsg', 'EventsController@sendmsg');

Route::post('/sendmsgplateforme', 'EventsController@sendmsgplateforme')->name('sendmsg');

Route::post('/gateway', 'GatewayController@addgateway')->name('insertgateway');
Route::post('/deletegateway', 'GatewayController@Deletegateway')->name('deletegateway');


Route::get('/report/{groupe}', 'EventsController@getreport');

Route::get('/device/{id}', 'EventsController@getdevice');

Route::get('/groups', 'EventsController@getgroupe');

Route::get('/informationtelephone', 'GatewayController@index')->name('informationtelephone');

Route::get('/messagenosend', 'MessageController@messagenosend')->name('messagepasenvoye');

Route::get('/coupure', 'MessageController@coupure')->name('coupure');

Route::get('/userinfo', 'GatewayController@user')->name('userinfo');
Route::post('/deleteuser', 'GatewayController@deleteuser')->name('deleteuser');





Route::get('/ecran', 'AjaxController@ecran');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

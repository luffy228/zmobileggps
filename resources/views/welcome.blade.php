<!DOCTYPE html>
<html lang="en">
  <head>



    <title>Zmobile</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="design2/css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    @yield('css_special')
  </head>
  <body class="app sidebar-mini">
    <!-- Navbar-->
    <header class="app-header"><a class="app-header__logo" href="#">Zmobile</a>
      <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
      <!-- Navbar Right Menu-->
      <ul class="app-nav">

        <!--Notification Menu-->


        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
          <ul class="dropdown-menu settings-menu dropdown-menu-right">
            <li> <a class="dropdown-item" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();"><i class="fa fa-sign-out fa-lg"></i>
                 {{ __('Logout') }}
             </a>

             <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                 @csrf
             </form></li>
          </ul>
        </li>
      </ul>
    </header>
    <!-- Sidebar menu-->
    <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
    <aside class="app-sidebar">
      <div class="app-sidebar__user"><img class="app-sidebar__user-avatar" src="img/user.png" alt="User Image">
        <div>
          <p class="app-sidebar__user-name">{{Auth::user()->name}}</p>
        </div>
      </div>
      <ul class="app-menu">
      <li><a class="app-menu__item active" href="{{route('home')}}"><i class="app-menu__icon fa fa-pie-chart"></i><span class="app-menu__label">Message Envoyes</span></a></li>
      <li><a class="app-menu__item" href="{{route('messagepasenvoye')}}"><i class="app-menu__icon fa fa-pie-chart"></i><span class="app-menu__label">Message Non Envoyes</span></a></li>
      <li><a class="app-menu__item" href="{{'coupure'}}"><i class="app-menu__icon fa fa-pie-chart"></i><span class="app-menu__label">Coupure-Activation</span></a></li>
      @if (Auth::user()->type == "admin")
      <li><a class="app-menu__item" href="{{'informationtelephone'}}"><i class="app-menu__icon fa fa-pie-chart"></i><span class="app-menu__label">Information Telephone</span></a></li>
      <li><a class="app-menu__item" href="{{'register'}}"><i class="app-menu__icon fa fa-pie-chart"></i><span class="app-menu__label">Ajouter un utilisateur</span></a></li>
      <li><a class="app-menu__item" href="{{'userinfo'}}"><i class="app-menu__icon fa fa-pie-chart"></i><span class="app-menu__label">Liste utilisateur</span></a></li>
      @endif






      </ul>
    </aside>
    <main class="app-content">
        @yield('content')
    </main>
    <!-- Essential javascripts for application to work-->
    <script src="design2/js/jquery-3.3.1.min.js"></script>
    <script src="design2/js/popper.min.js"></script>
    <script src="design2/js/bootstrap.min.js"></script>
    <script src="design2/js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="design2/js/plugins/pace.min.js"></script>

      <script>
      localStorage.setItem('from',null);
      async function  SendSMSNotification(){

        urlsms = localStorage.getItem('url');

        const event = await fetch(urlsms+"/evenemt",
               {
                  method: "GET",
                    headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                    },
                });

        const eventresult = await event.json();


        countevent =eventresult["count"];
        const from = localStorage.getItem('from');

        if (countevent != 0) {
            const responsegroupes = await fetch(urlsms+"/groups",
               {
                  method: "GET",
                    headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                    },
                });
                const datagroupes= await responsegroupes.json();
                for (item of datagroupes) {
                    groupeiD = item["id"];

                    var urlreport =urlsms+"/report/"+groupeiD
                    const report = await fetch(urlreport,
                        {
                            method: "GET",
                                headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json',
                               },
                            });

                    const reportresult = await report.json();

                    const countreportresultat = reportresult.length;


                    for (reportresultat of reportresult) {

                        if (reportresultat["type"] == "geofenceExit") {


                            const deviceId = reportresultat["deviceId"];
                            var urldevice =urlsms+"/device/"+deviceId
                        deviceinfo = await fetch(urldevice,
                            {
                                method: "GET",
                                    headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json',
                                },
                            });
                            const deviceresultat = await deviceinfo.json();
                            console.log("Sorti dans la geofence")
                            const sendmsg = await fetch(urlsms+"/sendmsg",
                            {
                                    method: "POST",
                                    body: JSON.stringify({
                                        num:deviceresultat[0]["phone"],
                                        msg:555
                                    }),
                                    headers: {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json',
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                            });
                            const sendmsgresultat = await sendmsg.json();
                            console.log(sendmsgresultat);
                        }

                        if (reportresultat["type"] == "geofenceEnter") {

                            const deviceId = reportresultat["deviceId"];
                            var urldevice = urlsms+"/device/"+deviceId
                            deviceinfo = await fetch(urldevice,
                            {
                                method: "GET",
                                    headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json',

                                },
                            });

                            const deviceresultat = await deviceinfo.json();
                            console.log("Entrer dans la geofence")


                            const sendmsg = await fetch(urlsms+"/sendmsg",
                            {
                                    method: "POST",
                                    body: JSON.stringify({
                                        num:deviceresultat[0]["phone"],
                                        msg:666
                                    }),
                                    headers: {
                                    'Accept': 'application/json',
                                    'Content-Type': 'application/json',
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                            });

                            const sendmsgresultat = await sendmsg.json();
                            console.log(sendmsgresultat);




                        }


                    }



                }

        }


         if (countevent == 0) {

            if (from == "null") {
                console.log("rien dans le cache");
                var d = new Date();
                var date = d.getFullYear()+'-'+(d.getMonth()+1)+'-'+d.getDate();
                var hours = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds();
                var fullDate = date+' '+hours;
                localStorage.setItem('from',fullDate);
                const eventdate = await fetch(urlsms+"/addevent",
               {
                  method: "GET",
                    headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                    },
                });


            }

         }










            }

   setInterval(SendSMSNotification,60000);
</script>
    <!-- Page specific javascripts-->
    @yield('js_special')




  </body>
</html>

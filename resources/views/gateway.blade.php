@extends('welcome')



@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-th-list"></i>Information Telephone</h1>
        @if (count($gateway) == 0 )
        <p class="mt-3"><button class="btn btn-primary" data-toggle="modal"  data-id=# data-target="#AjoutModal">Ajout d'un Dispositif</button></p>
        @endif



        @if (session('success'))
        <div class="alert alert-success">
            {{session('success')}}
        </div>
      @endif
      @if (session('incorrect'))
        <div class="alert alert-warning">
            {{session('incorrect')}}
        </div>
      @endif
    </div>

    <ul class="app-breadcrumb breadcrumb side">
        <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
        <li class="breadcrumb-item">Zmobile</li>
        <li class="breadcrumb-item active"><a href="#">Dispositifs</a></li>
    </ul>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="tile-body">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th>DeviceID</th>
                                <th>Numero Sim</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                        @foreach ($gateway as $gateway)
                        <tr>
                        <td>{{$gateway["DeviceId"]}}</td>
                        <td>{{$gateway["Sim"]}}</td>
                            <td>
                                <span title="Supprimer"><button style="color: red" class="fa fa-trash-o deletemodal" data-toggle="modal"  data-id={{$gateway->id }}  data-target="#DeleteModal" ></button></span>

                            </td>


                        </tr>
                        @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="AjoutModal" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">

          <h4 class="modal-title">Ajouter un Gateway</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
    <form method="post" action="{{route('insertgateway')}}" >
           @csrf
        <div class="modal-body">



              <div class="form-group ">
                <label for="cname" class="control-label col-lg-10">Numero du DeviceId</label>
                <div class="col-lg-10">
                  <input class=" form-control" id="cname" name="deviceId" minlength="2" type="integer" required />
                </div>
              </div>

              <div class="form-group ">
                <label for="ccomment" class="control-label col-lg-10">Numero de la sim</label>
                <div class="col-lg-10">
                    <input class=" form-control" id="cname" name="sim" maxlength="1" type="integer" required />
                </div>
              </div>

              <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10 em">
                    <button data-dismiss="modal" class="btn btn-danger" type="reset">Annuler</button>
                    <button class="btn btn-primary" type="submit">Valider</button>
                </div>
              </div>

        </div>
        <div class="modal-footer">

        </div>
        </form>
      </div>
    </div>
</div>

<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="DeleteModal" class="modal fade">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Supprimercet numero</h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <form method="post" action="{{route('deletegateway')}}">
           @csrf
        <div class="modal-body">

              <input type="hidden" class="form-control" name="id" id="idk1" value="">


        </div>
        <div class="modal-footer">
          <button class="btn btn-danger" type="submit">Etes vous sur de vouloir supprimer cet numero?</button>
        </div>
        </form>
      </div>
    </div>
</div>
@section('js_special')

<script type="text/javascript" src="design2/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="design2/js/plugins/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    $('#sampleTable').DataTable();
</script>
<script type="text/javascript">
    /* Formating function for row details */




      $(document).on("click", ".deletemodal", function () {
       var ids = $(this).attr('data-id');
       console.log(ids);
       $(".modal-body #idk1").val( ids );

      });


  </script>

@endsection

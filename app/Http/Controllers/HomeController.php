<?php

namespace App\Http\Controllers;

use App\Gateway;
use Illuminate\Http\Request;
use App\Http\Singleton;
use App\TraccarSession;
use GuzzleHttp\Client;
use App\Traccar;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $client = new Client();
        $traccar = new Traccar();


        $response = $client->request('POST', $traccar->getSMS_uri()."/gettoken", [
              'form_params' => [
                  'email' => $traccar->getTraccarUser(),
                  'pass' =>$traccar->getTraccarPass(),
              ]
          ]);

        $smsgate = json_decode($response->getBody())        ;
        $localuri = "http://localhost:8000";
        echo "<script>document.write(localStorage.setItem('url', '".$localuri."'))</script>";
        Cache::put('tokensms', $smsgate->token);
        $smstoken = Cache::get('tokensms');

        $responsedevice = $client->request('GET',$traccar->getSMS_uri()."/getalldevices" ,[
            'headers' => [
                           'content-type' => 'application/json'],
            'query' =>[
                         'token' =>$smstoken,
                        ],
         ]);
         $gateway = Gateway::all();
         if (count($gateway) == 0) {
            $message = [];
            return view("messagesend",compact("message"));
        } else{
            $deviceid = $gateway[count($gateway)-1]["DeviceId"];
            $sim = $gateway[count($gateway)-1]["Sim"];
            $responseevent = $client->request('GET', $traccar->getSMS_uri()."/getallsms",[
            'headers' => [
                'content-type' => 'application/json'],
             'query' =>[
                 'token' =>$smstoken,
                 'device_id' =>$deviceid,
                 'status' => 7,
             ],
         ]);
         $message = json_decode($responseevent->getBody(),true);
         return view("messagesend",compact("message"));
         }
    }
}

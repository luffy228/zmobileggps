@extends('welcome')



@section('content')

<div class="app-title">
    <div>
        <h1><i class="fa fa-th-list"></i> Message Envoyes</h1>



    </div>

    <ul class="app-breadcrumb breadcrumb side">
        <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
        <li class="breadcrumb-item">Zmobile</li>
        <li class="breadcrumb-item active"><a href="#">Message Envoyes</a></li>
    </ul>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="tile">
            <div class="tile-body">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <thead>
                            <tr>
                                <th>Envoye le </th>
                                <th>Numero destinataire</th>
                                <th>Message Envoye</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($message ) != 0)
                            @foreach ($message["smss"] as $message)

                            <tr>

                                <td>{{$message["senttime"]["date"]}}</td>
                                <td>{{$message["sendto"]}}</td>
                                <td>{{$message["body"]}}</td>

                            </tr>
                        @endforeach

                            @else
                            <tr>

                                <td colspan="3"><p class="text-center">Pas de message </p></td>


                            </tr>

                            @endif






                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



@section('js_special')

<script type="text/javascript" src="design2/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="design2/js/plugins/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    $('#sampleTable').DataTable();
</script>
<script type="text/javascript">
    /* Formating function for row details */







  </script>

@endsection
